
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * How the Game Plays
 * Installation


INTRODUCTION
------------

Current Maintainer: Morbus Iff <morbus@disobey.com>

Cardflay is a deck-building card game engine that supports multiple power
types, rarities, and colors, a web-based UI for creating cards and sets,
automated play and duel logging, and more. It provides these features through
Drupal's custom content types, fields, and vocabularies, as well as a robust
API to let developers create new card types and gameplay functionality.

The Cardflay engine ships with a simple ruleset and a number of card power
types by default, but you should not expect to be up and running "simply"
by installing the module, as no cards or sets are created automatically. It
will be up to the administrator to create their own sets and cards per their
unique vision. If you're planning on using the default ruleset and power
types, this process is described in "Installation", below. If you'd like
to code your own power types or rules, see cardflay.api.php.

Be aware that the shipped Cardflay is not real-time or interactive like you'd
expect to see from Flash, Unity, or JavaScript. It's "merely" an engine that
automates duels (in one Drupal page load) based on a user's deck.


HOW THE GAME PLAYS
------------------

 * A card is defined thusly:
   * Name (of the card)
   * Set (it belongs to)
   * Identifier (within the Set)
   * Rarity (Fixed, Common, Uncommon, Rare, Special)
   * Color (Colorless, ROYGB, Purple, Black, Silver)
   * Keywords (a taxonomy vocabulary)
   * Power type (damage, discard, heal, etc.)
   * Power value (a number)
   * Image (optional)
   * Body (flavor text, etc.)

 * Each player has a life total and a deck of cards.

 * A random player goes first and plays the top card from their deck.

 * The card's power type and power value determines the effect.

 * The turn ends and the next player plays the top card from their deck.

 * When one player qualifies for a win or lose condition, the game is over.

Simple, right? The goal behind Cardflay was to create a card game that could
be fully automated by code, without interrupting a player to ask which card
to play, which cards to target, etc. The skill lies in finding, earning, or
buying cards, and then creating a decent deck.

The following card power types are shipped with Cardflay:

 * SUPPORT: Support cards protect the player from damage.
 * DAMAGE: Deal X damage to an opponent.
 * DISCARD: Force your opponent to discard X cards.
 * HEAL: Heal yourself for X life.
 * KILL: Kill one of your opponent's supports or one of yours if they've none.
 * LEECH: Deal X damage to an opponent and heal yourself that amount.

The following win and lose conditions are shipped with Cardflay:

 * Opponent's deck empty at the start of their turn.
 * Opponent's life has been depleted.


INSTALLATION
------------

 1. Copy the cardflay/ directory to your sites/SITENAME/modules directory.

 2. Enable the Cardflay module at admin/modules. You'll also need to enable
    at least one Power Type module - failure to do so will prevent you from
    creating a card in the following steps.

 3. Set your desired permissions at admin/people/permissions.

 4. Head to node/add/cardflay-set and create a new card set. Similar to
    collectible card games like Magic: The Gathering, Cardflay sets organize
    your cards into collections, releases, expansions, etc.

 5. Create a new card at node/add/cardflay-card. A rarity of "Fixed" means
    the card will automatically be given to players if they don't already have
    one ("Fixed" in Collectible Card Gaming terms tends to mean "part of a
    starter set"). Unless you intend to give your players cards in some other
    manner, you should define a few Fixed cards to start. Colors are currently
    display-only and have no in-game effect. Keywords have no in-game effect
    unless an installed Power Type or external module listens for them. The
    power type and power values determine the card's effect during play. A
    card's body can be used for flavor text or further rules explanation.

 6. After creating a few cards ... [continue]

